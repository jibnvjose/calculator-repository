﻿using CalculatorRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DTO
{
    public class CalculationDTO
    {
        public int CalculationId { get; set; }
        public DateTime? CalculatedOn { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Operand> Operands { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
    }
}
