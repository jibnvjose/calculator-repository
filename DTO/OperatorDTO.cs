﻿using CalculatorRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DTO
{
    public class OperatorDTO
    {
        public int OperatorId { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Operation> Operations { get; set; }
    }
}
