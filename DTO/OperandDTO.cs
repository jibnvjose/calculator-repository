﻿using CalculatorRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DTO
{
    public class OperandDTO
    {
        public int OperandId { get; set; }
        public double? Value { get; set; }
        public int? CalculationId { get; set; }

        public virtual Calculation Calculation { get; set; }
    }
}
