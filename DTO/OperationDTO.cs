﻿using CalculatorRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DTO
{
    public class OperationDTO
    {
        public int OperationId { get; set; }
        public int? OperatorId { get; set; }
        public int? CalculationId { get; set; }

        public virtual Calculation Calculation { get; set; }
        public virtual Operator Operator { get; set; }
    }
}
