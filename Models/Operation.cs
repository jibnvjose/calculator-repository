﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CalculatorRepository.Models
{
    public partial class Operation
    {
        public int OperationId { get; set; }
        public int? OperatorId { get; set; }
        public int? CalculationId { get; set; }

        public virtual Calculation Calculation { get; set; }
        public virtual Operator Operator { get; set; }
    }
}
