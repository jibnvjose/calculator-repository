﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CalculatorRepository.Models
{
    public partial class Calculation
    {
        public Calculation()
        {
            Operands = new HashSet<Operand>();
            Operations = new HashSet<Operation>();
        }

        public int CalculationId { get; set; }
        public DateTime? CalculatedOn { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Operand> Operands { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
    }
}
