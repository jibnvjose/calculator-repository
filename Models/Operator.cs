﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CalculatorRepository.Models
{
    public partial class Operator
    {
        public Operator()
        {
            Operations = new HashSet<Operation>();
        }

        public int OperatorId { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Operation> Operations { get; set; }
    }
}
