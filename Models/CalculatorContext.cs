﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CalculatorRepository.Models
{
    public partial class CalculatorContext : DbContext
    {
        public CalculatorContext()
        {
        }

        public CalculatorContext(DbContextOptions<CalculatorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Calculation> Calculations { get; set; }
        public virtual DbSet<Operand> Operands { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-DPN7308F;Database=Calculator;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Calculation>(entity =>
            {
                entity.ToTable("Calculation");

                entity.Property(e => e.CalculationId).HasColumnName("CalculationID");

                entity.Property(e => e.CalculatedOn).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Operand>(entity =>
            {
                entity.ToTable("Operand");

                entity.Property(e => e.OperandId).HasColumnName("OperandID");

                entity.Property(e => e.CalculationId).HasColumnName("CalculationID");

                entity.HasOne(d => d.Calculation)
                    .WithMany(p => p.Operands)
                    .HasForeignKey(d => d.CalculationId)
                    .HasConstraintName("FK_Operand_Calculation");
            });

            modelBuilder.Entity<Operation>(entity =>
            {
                entity.ToTable("Operation");

                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.CalculationId).HasColumnName("CalculationID");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.HasOne(d => d.Calculation)
                    .WithMany(p => p.Operations)
                    .HasForeignKey(d => d.CalculationId)
                    .HasConstraintName("FK_Operation_Calculation");

                entity.HasOne(d => d.Operator)
                    .WithMany(p => p.Operations)
                    .HasForeignKey(d => d.OperatorId)
                    .HasConstraintName("FK_Operation_Operator");
            });

            modelBuilder.Entity<Operator>(entity =>
            {
                entity.ToTable("Operator");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Symbol)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
