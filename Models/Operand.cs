﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CalculatorRepository.Models
{
    public partial class Operand
    {
        public int OperandId { get; set; }
        public double? Value { get; set; }
        public int? CalculationId { get; set; }

        public virtual Calculation Calculation { get; set; }
    }
}
