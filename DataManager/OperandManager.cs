﻿using CalculatorRepository.DTO;
using CalculatorRepository.Models;
using CalculatorRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DataManager
{
    public class OperandManager : IDataRepository<Operand, OperandDTO>
    {
        private CalculatorContext _calculatorContext;
        public OperandManager(CalculatorContext calculatorContext)
        {
            _calculatorContext = calculatorContext;
        }
        public IEnumerable<Operand> GetAll()
        {
            return _calculatorContext.Operands
                .ToList();
        }
        public Operand Get(long id)
        {
            throw new NotImplementedException();
        }
        public OperandDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(Operand entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Operand entityToUpdate, Operand entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(Operand entity)
        {
            throw new NotImplementedException();
        }
    }
}
