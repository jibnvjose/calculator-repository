﻿using CalculatorRepository.DTO;
using CalculatorRepository.Models;
using CalculatorRepository.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DataManager
{
    public class CalculationManager: IDataRepository<Calculation, CalculationDTO>
    {
        private CalculatorContext _calculatorContext;
        public CalculationManager(CalculatorContext calculatorContext)
        {
            _calculatorContext = calculatorContext;
        }
        public IEnumerable<Calculation> GetAll()
        {
            return _calculatorContext.Calculations
                .Include(calculation => calculation.Operands)
                .ToList();
        }
        public Calculation Get(long id)
        {
            throw new NotImplementedException();
        }
        public CalculationDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(Calculation entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Calculation entityToUpdate, Calculation entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(Calculation entity)
        {
            throw new NotImplementedException();
        }
    }
}
