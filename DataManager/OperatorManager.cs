﻿using CalculatorRepository.DTO;
using CalculatorRepository.Models;
using CalculatorRepository.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DataManager
{
    public class OperatorManager : IDataRepository<Operator, OperatorDTO>
    {
        private CalculatorContext _calculatorContext;
        public OperatorManager(CalculatorContext calculatorContext)
        {
            _calculatorContext = calculatorContext;
        }
        public IEnumerable<Operator> GetAll()
        {
            return _calculatorContext.Operators
                .ToList();
        }
        public Operator Get(long id)
        {
            throw new NotImplementedException();
        }
        public OperatorDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(Operator entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Operator entityToUpdate, Operator entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(Operator entity)
        {
            throw new NotImplementedException();
        }
    }
}
