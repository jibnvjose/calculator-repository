﻿using CalculatorRepository.DTO;
using CalculatorRepository.Models;
using CalculatorRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatorRepository.DataManager
{
    public class OperationManager : IDataRepository<Operation, OperationDTO>
    {
        private CalculatorContext _calculatorContext;
        public OperationManager(CalculatorContext calculatorContext)
        {
            _calculatorContext = calculatorContext;
        }
        public IEnumerable<Operation> GetAll()
        {
            return _calculatorContext.Operations
                .ToList();
        }
        public Operation Get(long id)
        {
            throw new NotImplementedException();
        }
        public OperationDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(Operation entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Operation entityToUpdate, Operation entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(Operation entity)
        {
            throw new NotImplementedException();
        }
    }
}
